package Pipes;

public class Hello {
   public static void main(String[] args) {
      // Prints "Pipes.Hello, World" in the terminal window.
      System.out.println("Pipes.Hello World from GitLab Runner");
      System.out.println("Automatic commit of sigmotoa");
      System.out.println("Hi guys");

      Person p1=new Person("sigmotoa",29);
      System.out.println(p1.name);
    }
}